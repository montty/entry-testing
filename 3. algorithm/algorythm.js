/**
 * Vertice.
 */
class Vertice {
    /**
     * COnstruct a new Vertice.
     * @param object the value object
     * @param id the id
     */
    constructor(object, id) {
        this.id = id;
        this.value = object;
    }
}

/**
 * Edge.
 */
class Edge {
    /**
     * Construct an Edge.
     * @param u the first vertice
     * @param v the second vertice
     * @param weight the weight on the edge.
     */
    constructor(u, v, weight) {
        // vertice 1
        this.u = u;
        // vertice 2
        this.v = v;
        // weight
        // non zero
        // if not provided defaults to 1
        this.w = weight || 1;
    }
}

/**
 * Graph.
 * Supports adding bidirectional and monodirectional weighted edges.
 */
class Graph {
    /**
     * Create a new Graph.
     */
    constructor() {
        // vertices array, 1D array
        this.vertices = [];

        // adjacency matrix, 2D array
        this.adjMatrix = [];
    }

    /**
     * Add a new vertice.
     * @param object the vertice object
     * @return {vertice} the new vertice
     */
    addVertice(object) {
        const len = this.vertices.length;
        const newVertice = new Vertice(object, len);
        // add a vertice
        this.vertices.push(newVertice);
        // add a row
        let newRow = [];
        for (let i = 0; i < len; i++) newRow.push(new Edge(null, null));
        this.adjMatrix[len] = newRow;

        // add a column
        for (let j = 0; j < len; j++) this.adjMatrix[j].push(new Edge(null, null));
        return newVertice;
    }

    /**
     * Delete a vertice.
     * @param {vertice} v is a vertice reference
     */
    deleteVertice(v) {

        // id to delete
        const i = v.id;
        const len = this.vertices.length;

        // last id
        const j = len - 1;
        if (i < j) {
            // swap deleted row and column
            // with last row and column

            // move vertice reference
            this.vertices[i] = this.vertices[j];

            // change vertice id
            this.vertices[i].id = i;

            // move column
            for (let k = 0; k < i; k++) this.adjMatrix[k][i] = this.adjMatrix[k][j];
            for (let k = i; k < j; k++) this.adjMatrix[k][i] = this.adjMatrix[k + 1][j];

            // move row
            for (let k = 0; k < i; k++) this.adjMatrix[i][k] = this.adjMatrix[j][k];
            for (let k = i; k < j; k++) this.adjMatrix[i][k] = this.adjMatrix[j][k + 1];

        }

        // remove last vertice
        this.vertices.splice(j, 1);

        // remove last row
        this.adjMatrix.splice(j, 1);

        // remove last column
        for (let k = 0; k < j; k++) this.adjMatrix[k].splice(j, 1);

    }

    /**
     * Add edge.
     * @param u the first vertice
     * @param v the second one
     * @param weight the weight
     * @param monodirectional the monodirectional flag means that the new edge is a one way road
     * if you want to create a two-way road with different weights for each road, you should call
     * the method twice, swapping @param u and @param v, e.g. addEdge(u, v, 1.5, true); addEdge(v, u, 0.5, true);
     * @returns the new edge
     */
    addEdge(u, v, weight, monodirectional) {
        const i = u.id, j = v.id;
        let edge = new Edge(u, v, weight);
        this.adjMatrix[i][j] = edge;
        if (!monodirectional) {
            this.adjMatrix[j][i] = edge;
        }
        return edge;
    }

    /**
     * Delete the edge.
     * @param e the edge to delete
     */
    deleteEdge(e) {
        e.u = null;
        e.v = null;
    }

    * verticeIterator() {
        yield* this.vertices;
    }

    * edgeIterator() {
        const len = this.vertices.length;
        for (let i = 0; i < len; ++i) {
            const _e = this.adjMatrix[i];
            for (let j = i; j < len; ++j) {
                const e = _e[j];
                if (e.u === null) continue;
                yield e;
            }
        }
    }

    * edgesForAGivenVerticeIterator(v) {
        const i = v.id;
        for (let e of this.adjMatrix[i]) {
            if (e.u === null) continue;
            yield e;
        }
    }

    * verticesConnectedIterator(w) {
        for (let {u, v} of this.adjMatrix[w.id]) {
            if (u === null) continue;
            yield u === w ? v : u;
        }
    }

    * edges() {
        for (let e of this.edgeIterator()) yield [e.u, e.v, e];
    }

    * incident(v) {
        for (let e of this.edgesForAGivenVerticeIterator(v)) yield [e.u, e.v, e];
    }

    * ingoing(v) {
        for (let e of this.edgesForAGivenVerticeIterator(v)) yield [e.u === v ? e.v : e.u, v, e];
    }

    * outgoing(v) {
        for (let e of this.edgesForAGivenVerticeIterator(v)) yield [v, e.u === v ? e.v : e.u, e];
    }

    endpoints(e) {
        return [e.u, e.v];
    }

    getAdjacencyMatrix() {
        return this.adjMatrix;
    }

    getVertices() {
        return this.vertices;
    }

    /**
     * Reverse (converse,transpose) a Graph.
     * If Graph is symmetric, i.e. Edge(u,v) = Edge(v, u),
     * this does nothing.
     * The result is a new transposed graph.
     */
    reverse() {
        let newGraph = new Graph();
        newGraph.vertices = [...this.vertices];
        newGraph.adjMatrix = this.adjMatrix[0].map((edge, columnIndex) => this.adjMatrix.map((row, rowIndex) => row[columnIndex]));
        return newGraph;
    }
}

class QueueNode {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class Queue {
    constructor() {
        this.first = null;
        this.last = null;
        this.N = 0;
    }

    enqueue(item) {
        const oldLast = this.last;
        this.last = new QueueNode(item);
        if (oldLast != null) {
            oldLast.next = this.last;
        }
        if (this.first == null) {
            this.first = this.last;
        }
        this.N++;
    }

    dequeue() {
        if (this.first == null) {
            return;
        }

        const oldFirst = this.first;
        const item = oldFirst.value;
        this.first = oldFirst.next;

        if (this.first == null) {
            this.last = null;
        }

        this.N--;

        return item;
    }

    size() {
        return this.N;
    }

    isEmpty() {
        return this.N == 0;
    }

    toArray() {
        const result = [];
        let x = this.first;
        while (x != null) {
            result.push(x.value);
            x = x.next;
        }
        return result;
    }
}

class BreadthFirstSearch {
    /**
     * Construct BFS for a graph and starting vertice.
     * @param graph the graph
     * @param start the starting vertice
     */
    constructor(graph, start) {
        const vertices = graph.vertices;
        this.start = start;

        let visited = this.visited = [];
        let edgeTo = this.edgeTo = [];

        for (let v of vertices) {
            visited.push(false);
            edgeTo.push(null);
        }

        const queue = new Queue();
        queue.enqueue(start);

        visited[start.id] = true;
        let count = 1;

        while (!queue.isEmpty()) {
            let top = queue.dequeue();
            visited[top.id] = true;

            graph.vertices.forEach(vertice => {
                if (null !== graph.adjMatrix[top.id][vertice.id].u && !visited[vertice.id]) {
                    queue.enqueue(vertice);
                    visited[vertice.id] = true;
                    edgeTo[vertice.id] = graph.adjMatrix[top.id][vertice.id];
                    count++;
                }
            });
        }
        this.count = count;
    }

    /**
     * Is there a path from start to end?
     * @param end the end vertice
     * @returns true if path exists
     */
    hasPathTo(end) {
        return this.visited[v.id];
    }

    /**
     * What is a path from start to end?
     * @param end the end vertice
     * @returns path as an array of vertices from start to end
     * You can also create a method to view the weights and edges
     */
    pathTo(end) {
        const path = [];
        if (end.id == this.start.id) {
            path.push(end);
            return path;
        }

        for (let x = end; x.id != this.start.id; x = this.edgeTo[x.id].u) {
            path.unshift(x);
        }
        return path;
    }
}
